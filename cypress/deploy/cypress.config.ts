import { defineConfig } from "cypress";

export default defineConfig({
  viewportWidth: 1280,
  viewportHeight: 800,
  video: false,
  env: {
    get_podcasts: "**/toppodcasts",
    get_episodes: "**/episodes/**",
  },
  e2e: {
    baseUrl: "http://localhost:80",
  },
});
