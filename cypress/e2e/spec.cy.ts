import type {} from "cypress";

describe("Mind Sound", () => {
  before(() => {
    const podcasts = Cypress.env("get_podcasts");
    const episodes = Cypress.env("get_episodes");
    // ----------------------------------------------------
    cy.intercept("GET", podcasts).as("toppodcasts");
    cy.intercept("GET", episodes).as("episodes");
    cy.visit("/");
  });

  const msShort = 1000;

  describe("Home", () => {
    const n = Math.floor(Math.random() * 5);
    it("The user searches for a podcast to listen to", () => {
      /* eslint-disable-next-line */
      cy.wait("@toppodcasts");
      cy.get("[data-testid=search-input]").should("to.exist");
      cy.get("[data-testid=search-input]").clear();
      /* eslint-disable cypress/no-unnecessary-waiting */
      cy.wait(msShort).then(() => {
        cy.get("[data-testid=search-input]").type("p");
      });
      cy.get("[data-testid=available-podcasts]  li").eq(n).should("to.exist");
      cy.get("[data-testid=available-podcasts]  li").eq(n).click();
      cy.wait("@episodes").then((result) => {
        cy.log("[result ]", result);
        cy.get("[data-testid=episodes-list]").should("to.exist");
        cy.get("[data-testid=episodes-list] tr").eq(2).should("to.exist");
        cy.get("[data-testid=episodes-list] tr").eq(2).click();
        cy.get("[data-testid=audio-ctrls]").should("to.exist");
        cy.get("[data-testid=podcast-detail]").should("to.exist");
        cy.get("[data-testid=navigate-to-podcast]").click();
        cy.get("[data-testid=navigate-to-home]").should("to.exist");
        cy.get("[data-testid=navigate-to-home]").click();
        cy.get("[data-testid=podcast-thumbnail]").eq(n).should("to.exist");
        cy.get("[data-testid=podcast-thumbnail]").eq(n).click();
      });
    });
  });
});
