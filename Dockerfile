#!/usr/bin/env -S docker build --no-cache --tag=prod/mind-sound . --network=host --file
# build a production version
#

FROM node:18-alpine3.15 as build-stage
ARG now
ENV build_date=$now
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY ./ .
RUN ls -a
RUN npm run check:prj
RUN npm run build
