export const Nothing = Symbol("nothing");
type Nothing = typeof Nothing;

export type Maybe<T> = T | Nothing;

type DoPodSearchByStr = {
  type: "DoPodSearchByStr";
  payload: string;
};

type View = "Home" | "Podcast" | "Episode";

type SelectView = {
  type: "SelectView";
  payload: {
    view: View;
    id: Maybe<number>;
  };
};

type NavigateTo = {
  type: "NavigateTo";
  payload: {
    url: string;
  };
};

type UpdatePodcasts = {
  type: "UpdatePodcasts";
  payload: Podcast;
};

type UpdatePodcastEpisodes = {
  type: "UpdatePodcastEpisodes";
  payload: {
    id: PodId;
    data: Episodes;
  };
};

type LoadingData = {
  type: "LoadingData";
};

type LocationChange = {
  type: "LocationChange";
  payload: {
    podcastId: Maybe<number>;
    episodeId: Maybe<number>;
  };
};

type None = { type: "None" };
export type Action =
  | NavigateTo
  | LoadingData
  | DoPodSearchByStr
  | SelectView
  | UpdatePodcasts
  | UpdatePodcastEpisodes
  | LocationChange
  | None;

export type PodId = number;
type EpisodeId = number;

// ------------------------
export type StorageData = Maybe<string>;

// ------------------------
//
export type Dispatch = (action: Action) => void;

export type SearchResult = {
  id: PodId;
  label: string;
};

export type Episode = {
  trackId: number;
  trackName: string;
  duration: string;
  releaseDate: string;
  description: string;
  audioType: string;
  audioSrc: string;
};

type Dict<T> = Record<string, T>;

export type Episodes = {
  ids: number[];
  data: Dict<Episode>;
};

export type Pod = {
  id: PodId;
  title: string;
  artist: string;
  description: string;
  img: string;
  episodes: Episodes;
};
export type PodImg = {
  label: string;
  height: number;
};

export type SearchBy = {
  id: Maybe<PodId>;
  title: string;
  artist: string;
};

export type PodUi = Pod & {
  dispatch: Dispatch;
};

export type Podcast = {
  data: Dict<Pod>;
  ids: PodId[];
};

export interface IModel {
  podcasts: Podcast;
  selectedPodcastId: Maybe<PodId>;
  selectedEpisodeId: Maybe<EpisodeId>;
  filterPodInput: string;
  searchList: SearchBy[];
  searchResult: SearchResult[];
  isLoadingData: boolean;
  isSearchFilterVisible: boolean;
  shouldGetPodcastFromApi: boolean;
  navigateTo: {
    url: string;
  };
}

export interface IModelUi extends IModel {
  dispatch: Dispatch;
}
