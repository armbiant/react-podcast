import { IModel, Action, Maybe, Pod, Nothing } from "../types";
import {
  createSearchList,
  setSearchResult,
  addEpisodesToPodcast,
  getPodcastById,
} from "../utils/helpers";

export function getInitialState(): IModel {
  return {
    podcasts: {
      data: {},
      ids: [],
    },
    selectedPodcastId: Nothing,
    selectedEpisodeId: Nothing,
    filterPodInput: "",
    searchList: [],
    searchResult: [],
    isSearchFilterVisible: true,
    shouldGetPodcastFromApi: false,
    isLoadingData: false,
    navigateTo: { url: "" },
  };
}

export function stateReducer(state: IModel, action: Action): IModel {
  switch (action.type) {
    case "None":
      return state;
    //
    case "NavigateTo":
      return { ...state, navigateTo: { url: action.payload.url } };
    //
    case "LoadingData":
      return {
        ...state,
        isLoadingData: true,
      };
    //
    case "SelectView":
      return (() => {
        if (action.payload.view === "Home") {
          return {
            ...state,
            selectedPodcastId: Nothing,
            selectedEpisodeId: Nothing,
            isSearchFilterVisible: true,
            navigateTo: {
              url: "/",
            },
          };
        }

        if (action.payload.view === "Podcast") {
          const id = action.payload.id;

          if (id !== Nothing) {
            const podcastFomState: Maybe<Pod> = getPodcastById(
              id,
              state.podcasts.data
            );

            const isPodcast = podcastFomState !== Nothing;

            if (isPodcast) {
              if (podcastFomState.episodes.ids.length) {
                return {
                  ...state,
                  selectedPodcastId: id,
                  selectedEpisodeId: Nothing,
                  searchResult: [],
                  filterPodInput: "",
                  isSearchFilterVisible: false,
                  shouldGetPodcastFromApi: false,
                  navigateTo: {
                    url: `/podcast/${id}`,
                  },
                };
              } else {
                return {
                  ...state,
                  selectedPodcastId: id,
                  selectedEpisodeId: Nothing,
                  searchResult: [],
                  filterPodInput: "",
                  isSearchFilterVisible: false,
                  shouldGetPodcastFromApi: true,
                  navigateTo: {
                    url: `/podcast/${id}`,
                  },
                };
              }
            }
          } else {
            console.error(
              "Expected selectedPodcastId to be a number, got Nothing"
            );
          }
        }

        if (action.payload.view === "Episode") {
          const selectedPodcastId =
            state.selectedPodcastId !== Nothing ? state.selectedPodcastId : "";

          const id = action.payload.id;

          if (id !== Nothing) {
            return {
              ...state,
              selectedEpisodeId: id,
              isSearchFilterVisible: false,
              navigateTo: {
                url: `/podcast/${selectedPodcastId}/episode/${id}`,
              },
            };
          } else {
            console.error(
              "Expected selectedEpisodeId to be a number, got Nothing"
            );
          }
        }
        return state;
      })();
    //
    case "LocationChange":
      return {
        ...state,
        selectedPodcastId: action.payload.podcastId,
        selectedEpisodeId: action.payload.episodeId,
        isSearchFilterVisible:
          action.payload.podcastId === Nothing &&
          action.payload.episodeId === Nothing,
      };

    case "UpdatePodcasts":
      return {
        ...state,
        podcasts: action.payload,
        searchList: createSearchList(action.payload),
        isLoadingData: false,
      };
    //
    case "UpdatePodcastEpisodes":
      return {
        ...state,
        isLoadingData: false,
        podcasts: addEpisodesToPodcast(
          state.podcasts,
          action.payload.data,
          action.payload.id
        ),
      };

    case "DoPodSearchByStr":
      return {
        ...state,
        filterPodInput: action.payload,
        searchResult: setSearchResult(state.searchList, action.payload),
      };
    //
    default:
      return state;
  }
}
