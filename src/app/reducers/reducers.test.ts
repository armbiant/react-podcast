import { getInitialState, stateReducer } from "./reducers";
import {
  IModel,
  Action,
  Nothing,
  SearchBy,
  SearchResult,
  Podcast,
  Maybe,
} from "../types";
import {
  PodcastWithOneAuthor,
  PodcastWithNoEpisodes,
  PodcastDetail,
} from "../../mocks/Fixtures";

const getPodcastWithNoEpisodes = () =>
  JSON.parse(JSON.stringify(PodcastWithNoEpisodes));

test("Given None, should return the state with no changes", () => {
  const action: Action = { type: "None" };
  const expected = getInitialState();
  const actual = stateReducer(getInitialState(), action);
  expect(actual).toMatchObject(expected);
});

test("Given an string should update route", () => {
  const action: Action = {
    type: "NavigateTo",
    payload: {
      url: "/",
    },
  };

  let actual = getInitialState();
  expect(actual.navigateTo.url).toBe("");

  actual = stateReducer(getInitialState(), action);
  expect(actual.navigateTo.url).toBe("/");
});

test("Given user input, should update filterPodInput", () => {
  const action: Action = {
    type: "DoPodSearchByStr",
    payload: "my str",
  };

  let actual = getInitialState();
  expect(actual.filterPodInput).toBe("");

  actual = stateReducer(getInitialState(), action);
  expect(actual.filterPodInput).toBe("my str");
});

test("Given a Podcast should update searchList", () => {
  // will be used at the time of searching
  const action: Action = {
    type: "UpdatePodcasts",
    payload: PodcastWithOneAuthor,
  };

  let actual = getInitialState();
  let expected: SearchBy[] = [];

  expect(actual.searchList).toMatchObject(expected);

  actual = stateReducer(getInitialState(), action);
  expected = [
    {
      id: 2222,
      title: "Joe Budden Podcast",
      artist: "The Joe Budden Network",
    },
  ];

  expect(actual.searchList).toMatchObject(expected);
});
//
test("Given a Podcast should update podcasts", () => {
  const action: Action = {
    type: "UpdatePodcasts",
    payload: PodcastWithOneAuthor,
  };

  let actual = getInitialState();
  let expected: Podcast = { data: {}, ids: [] };

  expect(actual.podcasts).toMatchObject(expected);

  actual = stateReducer(getInitialState(), action);
  expected = PodcastWithOneAuthor;

  expect(actual.podcasts).toMatchObject(expected);
});
//
test("Given an Episodes list, should update podcast episodes", () => {
  const action: Action = {
    type: "UpdatePodcastEpisodes",
    payload: {
      id: 2222,
      data: PodcastDetail.episodes,
    },
  };

  const state: IModel = {
    ...getInitialState(),
    podcasts: getPodcastWithNoEpisodes(),
  };

  let actual = state.podcasts.data[2222].episodes.ids.length;
  let expected = 0;

  expect(actual).toBe(expected);

  actual = stateReducer(state, action).podcasts.data[2222].episodes.ids.length;
  expected = 1;

  expect(actual).toBe(expected);
});
//
test("Given a string, it should update searchResult", () => {
  //
  const action: Action = {
    type: "DoPodSearchByStr",
    payload: "budd",
  };

  let actual = getInitialState();
  let expected: SearchResult[] = [];

  expect(actual.searchResult).toMatchObject(expected);

  actual = stateReducer(
    {
      ...getInitialState(),
      searchList: [
        {
          id: 2222,
          title: "Joe Budden Podcast",
          artist: "The Joe Budden Network",
        },
      ],
    },
    action
  );

  expected = [
    {
      id: 2222,
      label: "Joe Budden Podcast",
    },
  ];

  expect(actual.searchResult).toMatchObject(expected);
});

test("When we are loding data", () => {
  const action: Action = {
    type: "LoadingData",
  };

  let actual = getInitialState();

  expect(actual.isLoadingData).toBe(false);

  actual = stateReducer(getInitialState(), action);

  expect(actual.isLoadingData).toBe(true);
});
//
test("Given SelectView should update, selectedPodcastId, selectedEpisodeId", () => {
  // Home
  let action: Action = {
    type: "SelectView",
    payload: {
      view: "Home",
      id: Nothing,
    },
  };

  let actual = getInitialState();
  let expected = {
    ...getInitialState(),
    selectedPodcastId: Nothing,
    selectedEpisodeId: Nothing,
    isSearchFilterVisible: true,
    navigateTo: { url: "" },
  };
  expect(actual).toStrictEqual(expected);

  actual = stateReducer(
    {
      ...getInitialState(),
      selectedPodcastId: 222,
      selectedEpisodeId: 5535,
    },
    action
  );

  expected = {
    ...getInitialState(),
    selectedPodcastId: Nothing,
    selectedEpisodeId: Nothing,
    isSearchFilterVisible: true,
    navigateTo: { url: "/" },
  };

  expect(actual).toStrictEqual(expected);

  // Podcast
  action = {
    type: "SelectView",
    payload: {
      view: "Podcast",
      id: 2222,
    },
  };

  let id: Maybe<number> = 2222;
  actual = {
    ...getInitialState(),
  };

  expected = {
    ...getInitialState(),
    selectedPodcastId: Nothing,
    selectedEpisodeId: Nothing,
  };

  expect(actual).toStrictEqual(expected);

  actual = stateReducer(
    {
      ...getInitialState(),
      podcasts: getPodcastWithNoEpisodes(),
      filterPodInput: "my search input",
      searchResult: [{ id: 222, label: "my label" }],
    },
    action
  );

  expect(actual.filterPodInput).toBe("");
  expect(actual.selectedPodcastId).toBe(id);
  expect(actual.searchResult.length).toBe(0);

  // Episode
  action = {
    type: "SelectView",
    payload: {
      view: "Episode",
      id: 5535,
    },
  };

  actual = getInitialState();
  expected = {
    ...getInitialState(),
    selectedPodcastId: Nothing,
    selectedEpisodeId: Nothing,
  };

  expect(actual).toMatchObject(expected);

  actual = stateReducer(
    {
      ...getInitialState(),
      selectedPodcastId: 222,
    },
    action
  );

  id = 222;
  const episodeId: Maybe<number> = 5535;

  expect(actual.selectedPodcastId).toBe(id);
  expect(actual.selectedEpisodeId).toBe(episodeId);
});

test("set to true shouldGetPodcastFromApi, if the Podcasts are empty", () => {
  //
  const action: Action = {
    type: "SelectView",
    payload: {
      view: "Podcast",
      id: 2222,
    },
  };

  let actual = getInitialState();
  expect(actual.shouldGetPodcastFromApi).toBe(false);

  actual = stateReducer(
    {
      ...getInitialState(),
      podcasts: getPodcastWithNoEpisodes(),
    },
    action
  );

  expect(actual.shouldGetPodcastFromApi).toBe(true);
});

test("Search filter should be visible only at 'Home' view", () => {
  let action: Action = {
    type: "SelectView",
    payload: {
      view: "Podcast",
      id: 2222,
    },
  };

  let actual = getInitialState();
  expect(actual.isSearchFilterVisible).toBe(true);

  actual = stateReducer(
    {
      ...getInitialState(),
      podcasts: getPodcastWithNoEpisodes(),
    },
    action
  );

  expect(actual.isSearchFilterVisible).toBe(false);

  action = {
    type: "SelectView",
    payload: {
      view: "Episode",
      id: 5535,
    },
  };

  actual = getInitialState();
  expect(actual.isSearchFilterVisible).toBe(true);
  //
  actual = stateReducer(
    {
      ...getInitialState(),
    },
    action
  );

  expect(actual.isSearchFilterVisible).toBe(false);
  //
  action = {
    type: "SelectView",
    payload: {
      view: "Home",
      id: Nothing,
    },
  };

  actual = getInitialState();
  expect(actual.isSearchFilterVisible).toBe(true);
});
