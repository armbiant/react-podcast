import React, { useReducer, useEffect } from "react";

import {
  Route,
  Routes,
  useNavigate,
  useLocation,
  matchPath,
} from "react-router-dom";
import Header from "../components/header/Header";
import MainView from "../components/views/MainView";
import PodcastDetailView from "../components/views/PodcastDetailView";
import EpisodeDetailView from "../components/views/EpisodeDetailView";
import Footer from "../components/footer/Footer";
import { getInitialState, stateReducer } from "./reducers/reducers";
import {
  doOnStart,
  getEpisodes,
  numberFromString,
  copyObject,
} from "./utils/helpers";
import { IModel, Maybe, Nothing, Episodes } from "./types";

const isDevelop = process.env.NODE_ENV === "development";

const URL_TOP_PODCASTS = isDevelop
  ? "/toppodcasts/data.json"
  : "https://server-mind-sound.onrender.com/toppodcasts";

const URL_EPISODES = isDevelop
  ? "/episodes/123/data.json"
  : "https://server-mind-sound.onrender.com/episodes/{{id}}";

function App() {
  const [state, dispatch] = useReducer(stateReducer, getInitialState());
  const navigate = useNavigate();
  const location = useLocation();

  // get intial data from Api
  useEffect(() => {
    dispatch({
      type: "LoadingData",
    });
    doOnStart(URL_TOP_PODCASTS).then((data) => {
      dispatch({
        type: "UpdatePodcasts",
        payload: data,
      });
    });
  }, []);
  //
  useEffect(() => {
    if (state.navigateTo.url) {
      dispatch({
        type: "NavigateTo",
        payload: {
          url: "",
        },
      });
      navigate(state.navigateTo.url);
    }
  }, [state.navigateTo.url, navigate]);
  //
  useEffect(() => {
    const { pathname } = location;

    const matchPodcast = matchPath("/podcast/:podcastId", pathname);
    const matchEpisode = matchPath(
      "/podcast/:podcastId/episode/:episodeId",
      pathname
    );

    const podcastId: Maybe<number> = numberFromString(
      matchPodcast?.params?.podcastId || ""
    );

    const episodeId: Maybe<number> = numberFromString(
      matchEpisode?.params?.episodeId || ""
    );

    const podcastEpisode: Maybe<number> = numberFromString(
      matchEpisode?.params?.podcastId || ""
    );

    dispatch({
      type: "LocationChange",
      payload: {
        podcastId: podcastEpisode !== Nothing ? podcastEpisode : podcastId,
        episodeId,
      },
    });
  }, [location]);
  //
  useEffect(() => {
    if (state.shouldGetPodcastFromApi) {
      const id: Maybe<number> = state.selectedPodcastId;

      if (id !== Nothing) {
        const url = URL_EPISODES.replace("{{id}}", id.toString());

        dispatch({
          type: "LoadingData",
        });

        getEpisodes(url).then((data: Episodes) => {
          dispatch({
            type: "UpdatePodcastEpisodes",
            payload: {
              id,
              data,
            },
          });
        });
      }
    }
  }, [state.shouldGetPodcastFromApi, state.selectedPodcastId]);

  //
  return (
    <div className="main-container">
      <Header {...(copyObject(state) as IModel)} dispatch={dispatch} />
      <Routes>
        <Route
          path="/"
          element={
            <MainView {...(copyObject(state) as IModel)} dispatch={dispatch} />
          }
        />
        <Route
          path="/podcast/:podcastId"
          element={
            <PodcastDetailView
              {...(copyObject(state) as IModel)}
              dispatch={dispatch}
            />
          }
        />
        <Route
          path="/podcast/:podcastId/episode/:episodeId"
          element={
            <EpisodeDetailView
              {...(copyObject(state) as IModel)}
              dispatch={dispatch}
            />
          }
        />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
