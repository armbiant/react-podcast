import axios from "axios";
import {
  setItem,
  getItem,
  removeItem,
  getJsonFromStorage,
  STORAGE_PODCAST_DATA,
  getPodcatsFromLocalStorage,
  isLessThan24HsElapsed,
  doGet,
  doOnStart,
  getEpisodes,
} from "./helpers";

import { Maybe, Podcast, Episodes, Nothing } from "../types";

import {
  json_with_entries,
  dataFromStorage,
  entry,
  json_episodes,
  dataFromStorageLessThan24Hs,
} from "../../mocks/Fixtures";

jest.mock("axios");

const url = "/api/";

beforeEach(() => {
  // to fully reset the state between tests, clear the storage
  localStorage.clear();
  // and reset all mocks
  jest.clearAllMocks();
});

test("should save to localStorage", () => {
  const KEY = "foo",
    VALUE = "bar";
  setItem(KEY, VALUE);
  expect(localStorage.setItem).toHaveBeenLastCalledWith(KEY, VALUE);
  expect(localStorage.__STORE__[KEY]).toBe(VALUE);
  expect(Object.keys(localStorage.__STORE__).length).toBe(1);
});
//
test("should have remove item in the localStorage", () => {
  removeItem("foo");
  expect(localStorage.removeItem).toHaveBeenCalledTimes(1);
  expect(localStorage.__STORE__).toEqual({}); // check store values
  expect(localStorage.length).toBe(0); // or check length
});

test("should not impact the next test", () => {
  const KEY = "buzz",
    VALUE = "fizz";
  setItem(KEY, VALUE);
  expect(localStorage.setItem).toHaveBeenLastCalledWith(KEY, VALUE);
  expect(localStorage.__STORE__[KEY]).toBe(VALUE);
  expect(Object.keys(localStorage.__STORE__).length).toBe(1);
});
//
test("Given a key should get data from storage", () => {
  const KEY = "buzz",
    VALUE = "fizz";
  setItem(KEY, VALUE);
  expect(localStorage.setItem).toHaveBeenLastCalledWith(KEY, VALUE);
  expect(localStorage.__STORE__[KEY]).toBe(VALUE);
  expect(Object.keys(localStorage.__STORE__).length).toBe(1);

  const actual = getItem(KEY);

  expect(localStorage.getItem).toHaveBeenLastCalledWith(KEY);
  expect(actual).toBe(VALUE);
});
//
test("Given a key, should get Maybe string", () => {
  const KEY = STORAGE_PODCAST_DATA;
  const VALUE = "fizz";
  setItem(KEY, VALUE);
  expect(localStorage.setItem).toHaveBeenLastCalledWith(KEY, VALUE);
  expect(localStorage.__STORE__[KEY]).toBe(VALUE);
  expect(Object.keys(localStorage.__STORE__).length).toBe(1);

  const actual: Maybe<string> = getJsonFromStorage(KEY);
  expect(actual).toBe(VALUE);
});
//;
test("Given data Fom Storages as string should return Podcast", () => {
  const actual = getPodcatsFromLocalStorage(dataFromStorage);
  const expected: Podcast = {
    ids: [1535809341],
    data: {
      1535809341: {
        id: 1535809341,
        title: "The Joe Budden Podcast",
        artist: "The Joe Budden Network",
        description:
          "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends.",
        img: "im.png",
        episodes: { ids: [], data: {} },
      },
    },
  };

  expect(actual).toStrictEqual(expected);
});
//
test("given the time in milliseconds should return true if less than 24 hours have elapsed, otherwise false", () => {
  const h23 = 82800000;
  const h25 = 90000000;

  const now = window.performance.now();

  let actual = isLessThan24HsElapsed(h23);
  expect(actual).toBe(true);

  actual = isLessThan24HsElapsed(now);
  expect(actual).toBe(true);

  actual = isLessThan24HsElapsed(h25);
  expect(actual).toBe(false);
});

test("Given an url should get fetch data from Api", () => {
  const users = [{ name: "Bob" }];
  const resp = { data: [{ name: "Bob" }] };

  axios.get = jest.fn().mockResolvedValue(resp);

  doGet(url).then((data) => {
    expect(data).toStrictEqual(users);
  });

  expect(axios.get).toBeCalledWith(url);
});
//;
test("Should return Nothing when axios.get failed", () => {
  const error = "network error";
  const getError = new Error(error);
  axios.get = jest.fn().mockRejectedValue(getError);
  console.error = jest.fn();

  doGet(url).then((data) => {
    expect(console.error).toHaveBeenCalledWith(error);
    expect(data).toEqual(Nothing);
  });

  expect(axios.get).toBeCalledWith(url);
});
// //
test("Given an Url on start, should check we have data on storage and return Podcast", () => {
  const KEY = STORAGE_PODCAST_DATA;
  const VALUE = dataFromStorageLessThan24Hs;
  setItem(KEY, VALUE);
  expect(localStorage.setItem).toHaveBeenLastCalledWith(KEY, VALUE);
  expect(localStorage.__STORE__[KEY]).toBe(VALUE);
  expect(Object.keys(localStorage.__STORE__).length).toBe(1);

  const expected: Podcast = {
    ids: [1535809341],
    data: {
      1535809341: {
        id: 1535809341,
        title: "The Joe Budden Podcast",
        artist: "The Joe Budden Network",
        description:
          "Tune into Joe Budden and his friends. Follow along the crazy adventures of these very random friends.",
        img: "im.png",
        episodes: { ids: [], data: {} },
      },
    },
  };

  doOnStart(url).then((d) => {
    expect(d).toStrictEqual(expected);
  });
});
//
test("Given an Url on start, should call the Api to get Data", () => {
  axios.get = jest.fn().mockResolvedValue({ data: json_with_entries });

  doOnStart(url).then((data: Podcast) => {
    expect(data.ids.at(0)).toBe(1535809341);
  });

  expect(axios.get).toBeCalledWith(url);
});

test("Data from Service should be stored on localStorage", () => {
  const mockPerformanceMark = jest.fn();
  window.performance.now = mockPerformanceMark;

  const KEY = STORAGE_PODCAST_DATA;
  const resp = { data: json_with_entries };
  const VALUE = JSON.stringify({
    __date__: window.performance.now(),
    __data__: [entry],
  });

  axios.get = jest.fn().mockResolvedValue(resp);

  doOnStart(url).then(() => {
    expect(localStorage.setItem).toHaveBeenLastCalledWith(KEY, VALUE);
    expect(localStorage.__STORE__[KEY]).toStrictEqual(VALUE);
  });

  expect(axios.get).toBeCalledWith(url);
});
//
test("Given an url should get Episodes from Api", () => {
  axios.get = jest.fn().mockResolvedValue({
    data: json_episodes,
  });
  const trackId = 1000601100410;

  getEpisodes(url).then((data: Episodes) => {
    expect(data.ids.at(0)).toBe(trackId);
  });

  expect(axios.get).toBeCalledWith(url);
});
