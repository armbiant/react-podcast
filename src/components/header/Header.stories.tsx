import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import Header from "./Header";
import { FakeState } from "../../mocks/Fixtures";
export default {
  title: "Podcaster/Header",
  component: Header,
  argTypes: {
    dispatch: { action: "msg" },
  },
} as ComponentMeta<typeof Header>;

const Template: ComponentStory<typeof Header> = (args) => <Header {...args} />;

export const MainHeader = Template.bind({});
MainHeader.args = { ...FakeState };

export const MainHeaderShowUnderway = Template.bind({});
MainHeaderShowUnderway.args = { ...FakeState, isLoadingData: true };

export const MainHeaderSearchFilterIsEnabled = Template.bind({});
MainHeaderSearchFilterIsEnabled.args = {
  ...FakeState,
  isSearchFilterVisible: true,
};
