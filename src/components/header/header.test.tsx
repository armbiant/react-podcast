import * as React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import { FakeState } from "../../mocks/Fixtures";
import { IModelUi, Action, Nothing } from "../../app/types";
import Header from "./Header";

const defaultProps: IModelUi = { ...FakeState, dispatch: (x: Action) => x };

const msg: Action = {
  type: "SelectView",
  payload: {
    id: Nothing,
    view: "Home",
  },
};

function renderHeader(props: Partial<IModelUi>) {
  return render(<Header {...defaultProps} {...props} />);
}

describe("<Header> ", () => {
  test("Show loading data", async () => {
    renderHeader({});
    let loadingData = screen.queryByTestId("is-loading-data");
    expect(loadingData).toBeNull();

    renderHeader({ isLoadingData: true });
    loadingData = screen.queryByTestId("is-loading-data");
    expect(loadingData).toBeTruthy();
  });

  test("Should go to Home view", async () => {
    const dispatch = jest.fn();
    renderHeader({ dispatch });
    const goHome = await screen.findByTestId("navigate-to-home");
    fireEvent.click(goHome);
    expect(dispatch).toHaveBeenCalledWith(msg);
  });

  test("Search box is visible ", () => {
    renderHeader({});
    let searchBox = screen.queryByTestId("search-box");
    expect(searchBox).toBeNull();

    renderHeader({ isSearchFilterVisible: true });
    searchBox = screen.queryByTestId("search-box");
    expect(searchBox).toBeTruthy();
  });
});
