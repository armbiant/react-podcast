import React from "react";
import SearchPodByUser from "../searchBox/SearchPod";
import { IModelUi, Nothing } from "../../app/types";
export default function Header(props: IModelUi): JSX.Element {
  const { dispatch, isLoadingData, isSearchFilterVisible } = props;
  return (
    <div data-testid="header" className="header-container">
      <div className="header-top">
        <h2>
          <button
            data-testid="navigate-to-home"
            className="logo"
            onClick={() => {
              dispatch({
                type: "SelectView",
                payload: {
                  id: Nothing,
                  view: "Home",
                },
              });
            }}
          ></button>
        </h2>
        {isLoadingData && (
          <div
            data-testid="is-loading-data"
            className="header-top__podcast-underway"
          ></div>
        )}
      </div>
      <div className="header-bottom">
        {isSearchFilterVisible && <SearchPodByUser {...props} />}
      </div>
    </div>
  );
}
