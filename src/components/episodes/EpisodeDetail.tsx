import React from "react";
import { Episode } from "../../app/types";

export default function MainView({
  trackName,
  description,
  audioSrc,
}: Episode): JSX.Element {
  return (
    <div className="episode-detail box-shadow">
      <h1 className="title">{trackName}</h1>
      <p
        className="txt-color-grey font-style-italic"
        dangerouslySetInnerHTML={{ __html: description }}
      />
      {/* eslint-disable */}
      <audio controls data-testid="audio-ctrls">
        <source src={audioSrc} />
      </audio>
    </div>
  );
}
