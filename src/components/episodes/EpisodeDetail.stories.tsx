import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { PodEpisode } from "../../mocks/Fixtures";
import EpisodeDetail from "./EpisodeDetail";

export default {
  title: "Podcaster/EpisodeDetail",
  component: EpisodeDetail,
  argTypes: {
    dispatch: { action: "msg" },
  },
} as ComponentMeta<typeof EpisodeDetail>;

const Template: ComponentStory<typeof EpisodeDetail> = (args) => (
  <EpisodeDetail {...args} />
);

export const InitialEpisodeDetail = Template.bind({});
InitialEpisodeDetail.args = {
  ...PodEpisode,
};
