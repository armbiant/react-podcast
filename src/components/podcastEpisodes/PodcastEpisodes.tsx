import React from "react";
import { PodUi } from "../../app/types";

export default function PodcastEpisodes({
  episodes,
  dispatch,
}: PodUi): JSX.Element {
  return (
    <div className="episodes">
      <div className="episodes__result-count box-shadow">
        <h1 className="title">Episodes: {episodes.ids.length}</h1>
      </div>
      <table className="episodes__list box-shadow">
        <thead>
          <tr>
            <th>Title</th>
            <th>Date</th>
            <th>Duration</th>
          </tr>
        </thead>
        <tbody data-testid="episodes-list">
          {episodes.ids.map((id) => {
            const episode = episodes.data[id];
            if (!episode) {
              console.error(`can not find an episode with id: ${id}`);
            } else {
              const { trackId, trackName, duration, releaseDate } = episode;
              return (
                <tr
                  key={trackId}
                  onClick={() => {
                    dispatch({
                      type: "SelectView",
                      payload: {
                        id: trackId,
                        view: "Episode",
                      },
                    });
                  }}
                >
                  <td>{trackName}</td>
                  <td>{releaseDate}</td>
                  <td>{duration}</td>
                </tr>
              );
            }
          })}
        </tbody>
      </table>
    </div>
  );
}
