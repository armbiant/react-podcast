import React from "react";
import { PodUi } from "../../app/types";

export default function PodcastDetail({
  title,
  artist,
  description,
  img,
  dispatch,
}: PodUi): JSX.Element {
  return (
    <div data-testid="podcast-detail" className="podcast-detail">
      <button
        data-testid="navigate-to-podcast"
        onClick={() => {
          dispatch({ type: "None" });
        }}
      >
        <img alt={title} src={img} />
      </button>
      <button
        onClick={() => {
          dispatch({ type: "None" });
        }}
      >
        <div className="podcast-detail__author">
          <h2 className="title">{title}</h2>
          <p className="font-style-italic">{artist}</p>
        </div>
      </button>
      <div className="podcast-detail__description">
        <h2 className="subtitle-1">Description:</h2>
        <p className="txt-color-grey font-style-italic">{description}</p>
      </div>
    </div>
  );
}
