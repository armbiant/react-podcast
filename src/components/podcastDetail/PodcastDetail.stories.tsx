import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { PreviewPod } from "../../mocks/Fixtures";
import PodcastDetail from "./PodcastDetail";

export default {
  title: "Podcaster/PodcastDetail",
  component: PodcastDetail,
  argTypes: {
    dispatch: { action: "msg" },
  },
} as ComponentMeta<typeof PodcastDetail>;

const Template: ComponentStory<typeof PodcastDetail> = (args) => (
  <PodcastDetail {...args} />
);

export const InitialPodcastDetail = Template.bind({});
InitialPodcastDetail.args = {
  ...PreviewPod,
};
