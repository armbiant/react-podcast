import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import SearchPodByUser from "./SearchPod";
import { FakeState } from "../../mocks/Fixtures";
export default {
  title: "Podcaster/SearchPodByUser",
  component: SearchPodByUser,
  argTypes: {
    dispatch: { action: "msg" },
  },
} as ComponentMeta<typeof SearchPodByUser>;

const podcasts = {
  data: {},
  ids: new Array(100).fill("i"),
};

const Template: ComponentStory<typeof SearchPodByUser> = (args) => (
  <SearchPodByUser {...args} />
);

export const InitialSearcher = Template.bind({});
InitialSearcher.args = {
  ...FakeState,
  podcasts,
};

export const ShowResultOptions = Template.bind({});
ShowResultOptions.args = {
  ...FakeState,
  podcasts,
  searchResult: [
    { id: 2323, label: "metallica" },
    { id: 222, label: "monkey dum" },
  ],
};
