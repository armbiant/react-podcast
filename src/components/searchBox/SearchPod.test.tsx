import * as React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import { FakeState } from "../../mocks/Fixtures";
import { IModelUi, Action } from "../../app/types";
import SearchPod from "./SearchPod";

const defaultProps: IModelUi = { ...FakeState, dispatch: (x: Action) => x };
//

function renderSearchPod(props: Partial<IModelUi>) {
  const utils = render(<SearchPod {...defaultProps} {...props} />);
  const searchBox = screen.getByTestId("search-box");
  const input = screen.getByTestId("search-input");
  const podsNumber = screen.getByTestId("pods-number");
  return {
    searchBox,
    podsNumber,
    input,
    ...utils,
  };
}

describe("<SearchPod> ", () => {
  //
  test("Show the number of pods", () => {
    renderSearchPod({
      podcasts: { data: {}, ids: Array.from(Array(10).keys()) },
    });
    screen.getByText(/10/i);
  });

  test("The user searches for a pod using the Search Box input field", () => {
    const msg: Action = {
      type: "DoPodSearchByStr",
      payload: "s",
    };
    const dispatch = jest.fn();
    const { input } = renderSearchPod({ dispatch });
    fireEvent.change(input, { target: { value: "s" } });
    expect(dispatch).toHaveBeenCalledWith(msg);
  });

  test("Display a list of the available podcasts", () => {
    renderSearchPod({
      searchResult: [
        {
          id: 123,
          label: "Satoshi",
        },
      ],
    });

    screen.getByText(/satoshi/i);
  });

  test("The user selects a podcast from the list", async () => {
    const msg: Action = {
      type: "SelectView",
      payload: {
        view: "Podcast",
        id: 123,
      },
    };
    const dispatch = jest.fn();

    renderSearchPod({
      dispatch,
      searchResult: [
        {
          id: 123,
          label: "Satoshi",
        },
      ],
    });

    const availablePodcasts = await screen.findByTestId("select-podcast");
    fireEvent.click(availablePodcasts);
    expect(dispatch).toHaveBeenCalledWith(msg);
  });
});
