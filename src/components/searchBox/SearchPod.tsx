import React from "react";
import { IModelUi, SearchResult } from "../../app/types";

export default function SearchPodByUser({
  filterPodInput,
  dispatch,
  searchResult,
  podcasts,
}: IModelUi): JSX.Element {
  const podsNumber = podcasts.ids.length;
  return (
    <div data-testid="search-box" className="searcher">
      <span data-testid="pods-number" className="searcher__pods-number">
        {podsNumber}
      </span>
      <div className="searcher__search-box">
        <input
          data-testid="search-input"
          type="text"
          value={filterPodInput}
          placeholder="filter podcast"
          onChange={(evt) => {
            evt.preventDefault();
            dispatch({
              type: "DoPodSearchByStr",
              payload: evt.target.value || "",
            });
          }}
        />
        {searchResult.length >= 1 && (
          <ul data-testid="available-podcasts">
            {searchResult.map(({ id, label }: SearchResult) => (
              <li key={id}>
                <button
                  data-testid="select-podcast"
                  className="txt-color-grey"
                  onClick={() => {
                    dispatch({
                      type: "SelectView",
                      payload: {
                        view: "Podcast",
                        id,
                      },
                    });
                  }}
                >
                  {label}
                </button>
              </li>
            ))}
          </ul>
        )}
      </div>
    </div>
  );
}
