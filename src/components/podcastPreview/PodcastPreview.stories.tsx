import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { PreviewPod } from "../../mocks/Fixtures";
import PodcastPreview from "./PodcastPreview";

export default {
  title: "Podcaster/PodcastPreview",
  component: PodcastPreview,
  argTypes: {
    dispatch: { action: "msg" },
  },
} as ComponentMeta<typeof PodcastPreview>;

const Template: ComponentStory<typeof PodcastPreview> = (args) => (
  <PodcastPreview {...args} />
);

export const InitialPodcastPreview = Template.bind({});
InitialPodcastPreview.args = {
  ...PreviewPod,
};
