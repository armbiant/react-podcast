import * as React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import { PreviewPod } from "../../mocks/Fixtures";
import { PodUi, Action } from "../../app/types";
import PodcastPreview from "./PodcastPreview";

const defaultProps: PodUi = { ...PreviewPod, dispatch: (x: Action) => x };

const msg: Action = {
  type: "SelectView",
  payload: {
    id: 2222,
    view: "Podcast",
  },
};

function renderComponent(props: Partial<PodUi>) {
  return render(<PodcastPreview {...defaultProps} {...props} />);
}

describe("<PodcastPreview> ", () => {
  test("Title exist", () => {
    const podTitle = "Joe Budden Podcast";
    renderComponent({ ...PreviewPod, title: "" });
    expect(screen.queryByText(podTitle)).toBeNull();

    renderComponent({});
    screen.getByText(podTitle);
  });

  test("Artist exist", () => {
    const podArtist = "The Joe Budden Network";
    renderComponent({ ...PreviewPod, artist: "" });
    expect(screen.queryByText(podArtist)).toBeNull();

    renderComponent({});
    screen.getByText(podArtist);
  });

  test("Click to go back to Podcast View", async () => {
    const dispatch = jest.fn();

    renderComponent({ dispatch });
    const podcastThumbnail = await screen.findByTestId("podcast-thumbnail");
    fireEvent.click(podcastThumbnail);
    expect(dispatch).toHaveBeenCalledWith(msg);
  });
});
