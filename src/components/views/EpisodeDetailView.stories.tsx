import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { FakeState, Podcasts } from "../../mocks/Fixtures";
import EpisodeDetailView from "./EpisodeDetailView";

export default {
  title: "Podcaster/EpisodeDetailView",
  component: EpisodeDetailView,
  argTypes: {
    dispatch: { action: "msg" },
  },
} as ComponentMeta<typeof EpisodeDetailView>;

const Template: ComponentStory<typeof EpisodeDetailView> = (args) => (
  <EpisodeDetailView {...args} />
);

export const InitialEpisodeDetail = Template.bind({});
InitialEpisodeDetail.args = {
  ...FakeState,
  podcasts: { ...Podcasts },
  selectedPodcastId: 2222,
  selectedEpisodeId: 1535809342,
};
