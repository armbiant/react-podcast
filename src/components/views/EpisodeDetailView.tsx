import React from "react";
import PodcastDetail from "../podcastDetail/PodcastDetail";
import EpisodeDetail from "../episodes/EpisodeDetail";
import { IModelUi, Nothing } from "../../app/types";
import { getPodcastById, getEpisodeById } from "../../app/utils/helpers";

export default function EpisodeDetailView(props: IModelUi): JSX.Element {
  const { podcasts, selectedPodcastId, selectedEpisodeId, dispatch } = props;

  if (selectedPodcastId !== Nothing) {
    const podcastDetail = getPodcastById(selectedPodcastId, podcasts.data);

    if (podcastDetail !== Nothing) {
      const episodes = podcastDetail.episodes.data;

      if (selectedEpisodeId !== Nothing) {
        const episodeDetail = getEpisodeById(selectedEpisodeId, episodes);

        if (episodeDetail !== Nothing) {
          return (
            <div
              className="episode-detail-view-container container"
              data-testid="episode-view"
            >
              <PodcastDetail
                {...podcastDetail}
                dispatch={() => {
                  dispatch({
                    type: "SelectView",
                    payload: {
                      id: selectedPodcastId,
                      view: "Podcast",
                    },
                  });
                }}
              />
              <EpisodeDetail {...episodeDetail} />
            </div>
          );
        }
      }
    }
  }

  return <></>;
}
