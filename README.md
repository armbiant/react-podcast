# [Mind Sound](https://mind-sound.onrender.com/)

<p align="center">
  <a href="https://mind-sound.onrender.com/">
    <img src="https://user-images.githubusercontent.com/2205223/222253117-c0e2fb7e-3767-49ca-8bfe-13acf2333c13.svg" width="200" height="200" />
  </a>
</p>

### Front end technical exercise description:

The objective of this Exercise is to build an app to listen to podcasts. Content is retrieved from the Apple Itunes api.

## Clone the repo & install:

```
git clone "https://gitlab.com/suxxus1/podcast-react.git"
cd  ./podcast-react
npm install

```

## Usage:

You can develop by running the start command, which will activate the application on `port 4000`. Optionally, you can run the app in a Docker container on `port 3112`.

JSON will be obtained locally from `./public`. Production versions will fetch JSON from server API.

## Docker:

npm install if you haven't done it yet.

### Develop

```
cd ./podcast-react
docker compose up -d
docker logs <container name> -f
```

docker container name `dev__mind-sound`, port: `3112`

---

### Deploy

- Preview on local environment.

  You can run a build version in a local environment to verify that everything works as expected before deploying to the production environment.

run

```
docker compose -f docker-compose.prod.yml up --build -d
```

optionally as a shortcut, you can run the command `run-docker-prod.sh`

```
chmod +x run-docker-prod.sh
```

then use it as

```
./run-docker-prod.sh
```

docker container name `prod_mind-sound`

**_A build version will be displayed on localhost:80_**

---

### Gitlab-ci

- Run Gitlab-ci locally.

  Executing the Gitlab-ci pipelines in the local environment will allow you to set up `.gitlab-ci.yml`, and to verify that everything is working as intended.

**Register gitlab-runner**

```
docker run --rm -it \
-v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner \
register
```

optionally as a shortcut, you can run the command `register-gitlab-runner.sh`

```
chmod +x register-gitlab-runner.sh
```

then use it as

```
./register-gitlab-runner.sh
```

To register the gitlab-runner, you will be prompted for:

- GitLab instance URL -> `https://gitlab.com`
- registration token -> [token]
  you can obtain it:
  `https://gitlab.com/[your gitlab]/settings/ci_cd`, expand runners and copy the registration token

**Start a runner container**

```
docker run -d --rm --name gitlab-runner \
    -v $PWD:$PWD \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:latest
```

optionally as a shortcut, you can run the command `gitlab-runner.sh`

```
chmod +x gitlab-runner.sh
```

then use it as

```
./gitlab-runner.sh
```

docker container name `gitlab-runner`

**Execute the job**

```
docker exec -it -w $PWD gitlab-runner git config --global --add safe.directory "*" &&  docker exec -it -w $PWD gitlab-runner gitlab-runner exec docker [job name]
```

optionally as a shortcut, you can run the command `gitlab-execute.sh`

```
chmod +x gitlab-execute.sh
```

then use it, passing the job name as parameter

```
./gitlab-execute.sh [job name]
```

## Scripts:

The `package.json` file comes with the following scripts

- `husky-install` install husky.
- `storybook` to start storybook on `port:6006`.
- `start` to start development.
- `lint` to run ESLint.
- `tsc` to check _.ts _.tsx files.
- `tsc:watch` to watch _.ts _.tsx files.
- `test` to run jest suite.
- `test-app` to watch app \_.ts, \_.tsx tests.
- `test-components` to watch ./components\_.tsx tests.
- `cy:dev:open` run e2e test on Browser, used to create locally-run E2E tests against the dev\_\_mind-sound container on port 3112.
- `cy:dev:ci` run e2e cli, locally on port 3112.
- `cy:predeploy:ci` run e2e cli, is used to check locally executed E2E tests against the prod\_\_mind-sound container on port 80..
- `check:prj` run all check tasks lint typescript, jest tests.
- `build` to compile a version to be deployed.

## Resources:

[Storybook](https://storybook.js.org/)
Storybook is a frontend workshop for building UI components and pages in isolation.

[iTunes Search API](https://performance-partners.apple.com/search-api)
To search for content within the iTunes Store

## Demo online

[Mind Sound](https://mind-sound.onrender.com/)
